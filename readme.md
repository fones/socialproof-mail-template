# Create new template

1. Add folder template inside `src`
2. Create file `input.mjml` in that folder
3. Add images if necessary to that folder

# Develop

For better developing experience run

```
npm run dev --template=TEMPLATE_DIR
```

Replace `TEMPLATE_DIR` with name of you new template folder.

It will update output `index.html` file every time you change `input.mjml` file.

* You can place image inside template folder

# Build

To build template run

```
npm run build --template=TEMPLATE_DIR
```

where `TEMPLATE_DIR` is a name in `src` folder.

# Deploy

Fo deploy run

```
npm run deploy --template=TEMPLATE_DIR
```

it will create ZIP folder inside `/deploy` directory.